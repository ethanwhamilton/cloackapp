from calypso_backend.calypso_backend.backend import CalypsoToyBackend

Backend = CalypsoToyBackend()

# get drop down menu options
envs = Backend.ReturnOptions.environment()
print(envs)

file_types = Backend.ReturnOptions.file_type()
print(file_types)

models = Backend.ReturnOptions.model()
print(models)

# process a .exe file
file = open('sample_files/not_malware.exe', "rb").read()
output = Backend.predict(file, 'PE', 'Windows 7 32 bit', 'EMBER v1.0')
print(output)

# process a .pdf file
file = open('sample_files/not_malware.pdf', "rb").read()
output = Backend.predict(file, 'PDF', 'Windows 7 32 bit', 'EMBER v1.0')
print(output)
