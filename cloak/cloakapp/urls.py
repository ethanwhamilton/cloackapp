from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('newtest/', views.newtest, name='newtest'),
    path('deletetest/', views.deletetest, name='deletetest'),
    path('results/', views.results, name='results'),
]