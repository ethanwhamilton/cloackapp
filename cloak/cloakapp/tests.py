from django.test import TestCase
from .models import Environment, FileType, Model, Test


class CloackModelsTests(TestCase):
    def setUp(self):
        FileType.objects.create(type="sft", description="some filetype")
        Environment.objects.create(type="se", description="some environment")
        Model.objects.create(type="sm", description="some model")

    def test_foreign_keys_values_fine(self):
        """
        Check that the foreign key fields in the Test instance created here
        points to the right objects
        """
        file_type = FileType.objects.get(type="sft")
        environment = Environment.objects.get(type="se")
        model = Model.objects.get(type="sm")

        test = Test.objects.create(file_type=file_type,
                                   environment=environment,
                                   model=model)

        self.assertEqual(test.file_type.description, "some filetype")
        self.assertEqual(test.environment.description, "some environment")
        self.assertEqual(test.model.description, "some model")
