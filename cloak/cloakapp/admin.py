from django.contrib import admin

from .models import Test, FileType, Environment, Model

admin.site.register(Test)
admin.site.register(FileType)
admin.site.register(Environment)
admin.site.register(Model)