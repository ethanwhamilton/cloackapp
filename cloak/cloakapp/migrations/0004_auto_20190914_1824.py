# Generated by Django 2.2.5 on 2019-09-15 01:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cloakapp', '0003_delete_results'),
    ]

    operations = [
        migrations.CreateModel(
            name='Environment',
            fields=[
                ('type', models.CharField(max_length=7, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='FileType',
            fields=[
                ('type', models.CharField(max_length=3, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Model',
            fields=[
                ('type', models.CharField(max_length=7, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_name', models.CharField(max_length=30)),
                ('file_name', models.CharField(max_length=100)),
                ('score', models.DecimalField(decimal_places=5, max_digits=6)),
                ('result', models.BooleanField()),
                ('environment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='cloakapp.Environment')),
                ('file_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='cloakapp.FileType')),
                ('model', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='cloakapp.Model')),
            ],
        ),
        migrations.DeleteModel(
            name='Result',
        ),
    ]
