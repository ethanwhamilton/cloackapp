from django.db import models


class FileType(models.Model):
    type = models.CharField(
        max_length=3,
        primary_key=True
    )
    description = models.CharField(
        max_length=30,
    )


class Environment(models.Model):
    type = models.CharField(
        max_length=7,
        primary_key=True
    )
    description = models.CharField(
        max_length=30,
    )


class Model(models.Model):
    type = models.CharField(
        max_length=7,
        primary_key=True
    )
    description = models.CharField(
        max_length=30,
    )


class Test(models.Model):
    # We go with the automatic primary key

    # Field for test name
    test_name = models.CharField(
        max_length=30,
        blank=True,
        null=True,
    )

    # Field for file name
    file_name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
    )

    # Field for file type
    file_type = models.ForeignKey(
        FileType,
        models.SET_NULL,
        blank=True,
        null=True,
    )

    # Field for environment
    environment = models.ForeignKey(
        Environment,
        models.SET_NULL,
        blank=True,
        null=True,
    )

    # Field for model
    model = models.ForeignKey(
        Model,
        models.SET_NULL,
        blank=True,
        null=True,
    )

    # Field for score, decimal with 6 digits, 6 decimal places
    score = models.DecimalField(
        max_digits=6,
        decimal_places=5,
        blank=True,
        null=True,
    )

    # Field for result, binary
    result = models.BooleanField(
        blank=True,
        null=True,
    )
