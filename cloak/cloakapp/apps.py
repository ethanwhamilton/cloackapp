from django.apps import AppConfig


class CloakappConfig(AppConfig):
    name = 'cloakapp'
