from django import forms
from .models import FileType, Environment, Model


class DescriptiveModelChoiceField(forms.ModelChoiceField):
    # This customization makes the pull down menu show the description of the object
    # All "choice" models have a field called "description: for this purpose
    def label_from_instance(self, obj):
        return obj.description


class NewTestForm(forms.Form):
    test_name = forms.CharField(label='Test Name', max_length=30)
    file_type = DescriptiveModelChoiceField(label='File Type',
                                            queryset=FileType.objects.all().order_by('type'))
    environment = DescriptiveModelChoiceField(label='Environment',
                                              queryset=Environment.objects.all().order_by('type'))
    model = DescriptiveModelChoiceField(label='Model',
                                        queryset=Model.objects.all().order_by('type'))
    file = forms.FileField(label='Upload File')
