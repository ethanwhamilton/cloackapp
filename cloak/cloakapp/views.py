import logging
import time
import os
from django.http import HttpResponse
from django.template import loader
from lib.fullstackbackend.calypso_backend.calypso_backend.backend import CalypsoToyBackend
from .forms import NewTestForm
from .models import Test, FileType, Environment, Model


def index(request):
    template = loader.get_template('cloakapp/index.html')
    context = {}
    return HttpResponse(template.render(context, request))


def newtest(request):
    context = {}
    logger = logging.getLogger('django')
    if request.method == 'POST':
        # We get here through a POST request, most likely after getting data from the form
        start_new_test = time.time()
        new_test_form = NewTestForm(request.POST, request.FILES)
        if new_test_form.is_valid():
            new_test = Test()
            new_test.test_name = new_test_form['test_name'].value()
            new_test.file_type = FileType.objects.get(type=new_test_form['file_type'].value())
            new_test.environment = Environment.objects.get(type=new_test_form['environment'].value())
            new_test.model = Model.objects.get(type=new_test_form['model'].value())
            # Call the toy Calypso Backend
            start_calypso_backend_call = time.time()
            calypso_toy_backend = CalypsoToyBackend()
            file = request.FILES['file'].read()
            output = calypso_toy_backend.predict(file,
                                                 new_test.file_type.type,
                                                 new_test.environment.description,
                                                 new_test.model.description)
            end_calypso_backend_call = time.time()
            new_test.file_name = request.FILES['file'].name
            new_test.score = output['score']
            new_test.result = output['is_malware']
            start_write_to_database = time.time()
            new_test.save()
            end_write_to_database = time.time()
            end_new_test = time.time()
            time_new_test = 1000.0*(end_new_test - start_new_test)
            time_write_database = 1000.0*(end_write_to_database - start_write_to_database)
            time_calypso_backend_call = 1000.0*(end_calypso_backend_call - start_calypso_backend_call)
            if 'PRINT_MEASUREMENTS' in os.environ:
                if os.environ['PRINT_MEASUREMENTS'] == "true":
                    logger.info("time to run new test   = %.4f ms", time_new_test)
                    logger.info("time to call backend   = %.4f ms", time_calypso_backend_call)
                    logger.info("time to write into db  = %.4f ms", time_write_database)
            template = loader.get_template('cloakapp/newtestresult.html')
            return HttpResponse(template.render(context, request))
        else:
            template = loader.get_template('cloakapp/invalidtestdata.html')
            return HttpResponse(template.render(context, request))

    template = loader.get_template('cloakapp/newtest.html')
    new_test_form = NewTestForm()
    context = {
        'new_test_form': new_test_form
    }
    return HttpResponse(template.render(context, request))


def deletetest(request):
    test_id_to_delete = request.GET['testid']
    test_to_delete = Test.objects.get(id=test_id_to_delete)
    name_test_to_delete = test_to_delete.test_name
    Test.objects.filter(id=test_id_to_delete).delete()
    context = {
        'deleted_test_name': name_test_to_delete,
    }
    template = loader.get_template('cloakapp/testdeleted.html')
    return HttpResponse(template.render(context, request))


def results(request):
    # This is a simple heuristic for the POC
    # In a production setting, we would do something different to avoid a large query here
    latest_tests_list = Test.objects.order_by('id')[:10]
    template = loader.get_template('cloakapp/results.html')
    context = {
        # Reverse the order so that the last result shows up first
        'latest_tests_list': latest_tests_list[::-1],
    }
    return HttpResponse(template.render(context, request))
