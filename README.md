# cloackapp

C.L.O.A.K. Challenge for Calypso AI

To run the app:

- Move to a local directory, say `~/sites/calypsoai`
- In the directory, clone the app by running `git clone https://gitlab.com/ethanwhamilton/cloackapp.git`
- Move to `~/sites/calypsoai/cloackapp/cloak`
- Execute `python manage.py runserver`

You will see

`Watching for file changes with StatReloader`
`Performing system checks...`
` `
`System check identified no issues (0 silenced).`
`September 15, 2019 - 18:30:53`
`Django version 2.2.5, using settings 'cloak.settings'`
`Starting development server at http://127.0.0.1:8000/`
`Quit the server with CONTROL-C.`

Then you can access the following two apps:

- Admin, `http://127.0.0.1:8000/admin/`
- CLOAK App `http://127.0.0.1:8000/cloakapp/`